const array = ["Enter","s","e","o","n","l","z"];

window.addEventListener("keypress", (event) => {
    if (array.includes(event.key)) {
        blackAll();
        makeBlue(event.key);
    }

});

function blackAll() {
    document.querySelectorAll(".btn").forEach((elem) => {
        elem.style.backgroundColor = "black";
    });
}

function makeBlue(key) {
    document.querySelectorAll(".btn").forEach((elem) => {
        if (elem.getAttribute("data-button") === key) {
            elem.style.backgroundColor = "blue";
        }
    })
}